const express = require('express');
const router = express.Router();

const controller = require('./controller');

// use 
router.get('/', controller.baseRoute);

// create
router.post('/create', controller.createPost);

// read all
router.get('/getPosts', controller.getPosts);

// read one
router.get('/getPost/:id', controller.getSinglePost);

//update
router.put('/update/:id', controller.updatePost);

// delete
router.get('/delete/:id', controller.deletePost);

//read one article
router.get('/getbody/:pid/:aid', controller.getBody);

module.exports = router;