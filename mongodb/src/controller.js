const mongoose = require("mongoose");
const Posts = mongoose.model("posts");

exports.baseRoute = async (req, res) => {
    res.send("Server Running");
};

// function to get posts on route "/getPosts"
exports.getPosts = async (req, res) => {
    console.log("5000 riceve la chiamata");
    const posts = await Posts.find();
    //console.log(posts);
    res.json(posts);
};

// function to create a post
exports.createPost = async (req, res) => {
    //console.log(req.body);
    // we use mongodb's save functionality here
    await new Posts(req.body).save((err, data) => {
        if (err) {
            // if there is an error send the following response
            res.status(500).json({
                message:
                    "Something went wrong in the createPost at 5000, please try again later.",
            });
            console.log("sono 5000, non ha funzionato il createPost");
            console.log(err);
        } else {
            // if success send the following response
            res.status(200).json({
                message: "Post Created , 5000 ok",
                data,
            });
            console.log("post creato, sono il server 5000");
        }
    });
};

// function to get a single post
exports.getSinglePost = async (req, res) => {
    // get id from URL by using req.params
    let postID = req.params.id;
    // we use mongodb's findById() functionality here
    await Posts.findById({ _id: postID }, (err, data) => {
        if (err) {
            res.status(500).json({
                message:
                    "Something went wrong, please try again later.",
            });
        } else {
            console.log("a db è arrivata la richiesta di un singolo post");
            res.status(200).json(data);
        }
    });
};

// function to update a single post
exports.updatePost = async (req, res) => {
    //console.log(req.body.articles.length);
    let p = new Posts(req.body);
    //console.log(p.articles.length);
    p.isNew = false;
    p.save((err, data) => {
        if (err) {
            // if there is an error send the following response
            res.status(500).json({
                message: "update non funziona",
            });
        } else {
            // if success send the following response
            res.status(200).json(data);
        }
    });
}

// function to delete a post from the DB
exports.deletePost = async (req, res) => {
    let postID = req.params.id;
    // we use mongodb's deleteOne() functionality here
    await Posts.deleteOne({ _id: postID }, (err, data) => {
        if (err) {
            res.status(500).json({
                message:
                    "Something went wrong, please try again later.",
            });
            console.log("sono 5000, il delete non ha funzionato");
        } else {
            res.status(200).json({
                message: "Post Deleted"
            });
            console.log("Sono 5000, il post è cancellato");
        }
    });
};


exports.getBody = async (req, res) => {
    const id_post = req.params.pid;
    const id_art = req.params.aid;
    var post = await Posts.findOne({ "_id": mongoose.Types.ObjectId(id_post), "articles._id": mongoose.Types.ObjectId(id_art) }, 'articles').exec();
    if (post == null) {
        console.log("Sono 5000. Ci sta un errore nel findone");
        res.status(500);
    }
    //console.log(post);
    var art = post.articles.find(el => el._id == id_art);
    if (typeof (art) == undefined) {
        console.log("Sono 5000. Ci sta un errore dopo il findone");
        res.status(500);
    }
    //console.log("tutto ok, sono 5000");
    res.status(200).json(art);
}