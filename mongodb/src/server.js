const express = require('express'); // import express
const app = express();
const port = 5000;

app.use(express.json({ limit: '50mb' }));
app.use(express.urlencoded({ extended: true, limit: '50mb' }));
const mongoose = require('mongoose');
require('dotenv').config({ path: '.env' });

mongoose.connect(process.env.DATABASE,
    {
        useUnifiedTopology: true,
        useNewUrlParser: true
    });

mongoose.Promise = global.Promise;

mongoose.connection.on('error', (err) => {
    console.error(`Database connection error -> ${err.message}`);
});

require('./posts');
const routes = require('./routes');
//app.use(express.json());
//app.use(express.urlencoded({ extended: true }));


app.use('/', routes);


app.listen(port, () => console.log(`Web server listening on port ${port}!`))