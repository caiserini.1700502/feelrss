const mongoose = require('mongoose');

mongoose.Promise = global.Promise;

const ArticleSchema = new mongoose.Schema({
    /*_id: {
        type: mongoose.ObjectId
    },*/
    title: {
        type: String
    },
    link: {
        type: String
    },
    body: {
        type: String
    }
},
    { _id: true }
);

const PostsSchema = new mongoose.Schema({
    name: {
        type: String
    },
    link: {
        type: String
    },
    articles: {
        type: [ArticleSchema]
    }
});



let Posts = mongoose.model('posts', PostsSchema);
