const mongoose = require('mongoose');
mongoose.set('useFindAndModify', false);

const { render } = require('ejs');
//const fetch = require('node-fetch');
const superagent = require('superagent');

const Rss_Url = process.env.RSS_URL || "localhost"
const MONGODB_URL = process.env.MONGODB_URL || "localhost"



exports.homeRoute = async (req, res) => {
    superagent.get(`http://${MONGODB_URL}:5000/getPosts`).end((err, resp) => {
        if (err) { return console.log(err); }
        //console.log(resp.body);
        res.render('index', { data: resp.body });
    });
}

exports.newFront = async (req, res) => {
    res.render('new');
}

//function to get a list of articles
exports.getArticle = async (req, res) => {
    let art_id = req.params.id;

    superagent.get(`http://${Rss_Url}:4000/rss/${art_id}`)
        .end((err, resp) => {
            if (err) { return console.log(err); }
            console.log(resp.body);
            res.render('update', { data: resp.body });
        });

}

//function to read one article, send request to webserver
exports.readBody = async (req, res) => {
    const id_post = req.params.pid;
    const id_art = req.params.aid;
    superagent.get(`http://${MONGODB_URL}:5000/getbody/${id_post}/${id_art}`)
        .end((err, resp) => {
            if (err) { return console.log(err); }
            console.log(resp.body);
            res.render('read', { content: resp.body });
        });
}

//function to create a post, send request to webserver
exports.addPost = async (req, res) => {
    console.log(req.body);
    superagent.post(`http://${MONGODB_URL}:5000/create`)
        .send(req.body)
        .end((err, resp) => {
        if (err) { return console.log(err); }
        res.redirect('/');
    });
}

// function to delete a post, send request to webserver
exports.removePost = async (req, res) => {
    let postID = req.params.id;
    superagent.get(`http://${MONGODB_URL}:5000/delete/${postID}`)
        .end((err, resp) => {
            if (err) { return console.log(err); }
            console.log("il post è stato cancellato");
            res.redirect('/');

        });
};

