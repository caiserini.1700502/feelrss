const mongoose = require('mongoose');

mongoose.Promise = global.Promise;

const PostsSchema = new mongoose.Schema({
    name: {
        type: String
    },
    link: {
        type: String
    },
    articles: {
        type: Array
    }
});

let Posts = mongoose.model('posts', PostsSchema);