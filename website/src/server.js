
const mongoose = require('mongoose');

mongoose.Promise = global.Promise;
mongoose.connection.on('error', (err) => {
    console.error(`Database connection error -> ${err.message}`);
});

require('./Models/Posts');


const app = require('./app');

const server = app.listen(3000, () => {
    console.log(`Express running' -> PORT ${server.address().port}`);
    //console.log(__dirname + '/\public');
})