const express = require('express');
const router = express.Router();
const postController = require('../Controllers/PostController');

// first page 
router.get('/', postController.homeRoute);

// simply a route to 'new' page
router.get('/new', postController.newFront);

// get a list of articles
router.get('/update/:id', postController.getArticle);

//read one article
router.get('/read/:pid/:aid', postController.readBody);

// add new post
router.post('/addpost', postController.addPost);

// remove one post
router.get('/remove/:id', postController.removePost);

module.exports = router;