FROM rust:1.52.1-alpine3.12 as builder

RUN apk add libressl-dev pkgconf musl-dev

WORKDIR /usr/src
RUN USER=root cargo new rss_posts
WORKDIR /usr/src/rss_posts

COPY . .

RUN cargo build 
RUN cp target/debug/rss_posts /usr/local/cargo/bin/rss_posts


FROM alpine:3.12.7
RUN apk add libressl-dev
COPY --from=builder usr/local/cargo/bin/rss_posts /usr/local/bin/rss_posts
COPY Rocket.toml /data/
ENV ROCKET_CONFIG=/data/Rocket.toml

CMD ["rss_posts"]

EXPOSE 4000