
use serde::{Deserialize, Serialize};
use std::fmt::Display;

#[derive(Serialize, Deserialize, Debug)]
pub struct Post{
    #[serde(rename="_id")]
    id: String,
    name: String,
    link: String,
    articles: Vec<Article>, 
}

impl Post {
    /// Get a reference to the post's link.
    pub fn link(&self) -> &String {
        &self.link
    }

    /// Set the post's articles.
    pub fn set_articles(&mut self, articles: Vec<Article>) {
        self.articles = articles;
    }
}

#[derive(Serialize, Deserialize, Debug)]
pub struct Article{
    title: String,
    link: String,
    body: String,
}

impl Article {
    pub fn new(title: String, link: String, body: String) -> Self { Self { title, link, body } }
}

impl Display for Post {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let arts = self.articles.iter().map(|a| a.to_string()).collect::<Vec<_>>().join("\n\t");
        write!(f, "Name: {},\nLink: {},\nArticles: {}\n", self.name, self.link, arts)
    }
}

impl Display for  Article {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "Title: {},\nLink: {},\nBody: {}\n", self.title, self.link, self.body)
    }
}


#[derive(Serialize, Deserialize, Debug)]
pub struct ResPost{
    #[serde(rename="_id")]
    id: String,
    name: String,
    link: String,
    articles: Vec<ResArticle>, 
}

#[derive(Serialize, Deserialize, Debug)]
pub struct ResArticle{
    #[serde(rename="_id")]
    id: String,
    title: String,
    link: String,
    #[serde(skip_deserializing)]
    body: String,
}

