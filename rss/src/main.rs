//#![feature(proc_macro_hygiene, decl_macro)]

#[macro_use]
extern crate rocket;
extern crate reqwest;
extern crate rocket_contrib;
extern crate rss;
extern crate serde;
extern crate lazy_static;

mod schema;

use rocket::http::Status;

use rocket::Request;
use rocket::serde::json::Json;
use rocket::serde::{Serialize, Deserialize};
use rocket::futures::future::join_all;

use rss::Channel;

use reqwest::Client;

use std::str::FromStr;
use std::env;
use lazy_static::lazy_static;

use schema::{Article, Post, ResPost};

#[derive(Serialize, Deserialize)]
struct Html {
    link: String,
    body: String,
}

#[get("/rss/<oid>")]
async fn rss_id(oid: String) -> Result<Json<ResPost>, Status> {
    let client = Client::new();
    let post = client
        .get(format!("http://{}:5000/getPost/{}",*MONGODB_URL, oid))
        .send()
        .await;

    let post = match post {
        serde::__private::Result::Ok(p) => p.json::<Post>().await,
        serde::__private::Result::Err(_) => {
            return Err(Status::new(
                500, /* , "Could not connect to the database" */
            ));
        }
    };
    let mut post = match post {
        serde::__private::Result::Ok(p) => p,
        serde::__private::Result::Err(_) => {
            return Err(Status::new(404 /* , "No post found" */));
        }
    };
    let link = post.link();

    let content = client.get(link).send().await.unwrap();
    let content = content.text().await.unwrap();
    let channel = Channel::from_str(&content);
    match channel {
        Ok(ch) => {
            let articles = get_and_parse(ch).await;
            post.set_articles(articles);
            let res = client
                .put(format!("http://{}:5000/update/{}",*MONGODB_URL, oid))
                .json(&post)
                .send().await;
            match res {
                serde::__private::Result::Ok(res) => {
                    let p = match res.json::<ResPost>().await {
                        serde::__private::Result::Ok(p) => p,
                        serde::__private::Result::Err(_) => {
                            return Err(Status::new(
                                404, /* , "Could Not Deserialize response Post" */
                            ));
                        }
                    };
                    Ok(Json(p))
                }
                serde::__private::Result::Err(_) => {
                    Err(Status::new(
                        500, /* , "Could not insert new articles" */
                    ))
                }
            }
        }
        Err(_) => Err(Status::new(404 /* , "Rss link feed not found!" */)),
    }
}

async fn get_and_parse(rss: Channel) -> Vec<Article> {
    let client = Client::new();
    let mut requests = Vec::with_capacity(rss.items().len());
    for item in rss.items().iter().filter(|item| item.link().is_some()) {
        let title = item.title().unwrap();
        let link = item.link().unwrap();
        requests.push(get_article(title, link, &client));
    }
    join_all(requests).await
}

async fn get_article(title: &str, link: &str, client: &Client) -> Article {
    let source = match client.get(link).send().await {
        Ok(content) => content,
        Err(e) => {
            return Article::new(title.to_string(), link.to_string(), e.to_string());
        }
    };
    let body = source.text().await.unwrap();
    let html = Html{link : link.to_string(), body};
    let res = client
        .post(format!("http://{}:6000/parse", *PARSE_URL))
        .json(&html)
        .send()

        .await;
    match res {
        Ok(r) => {
            let body = r.text().await.unwrap();
            Article::new(title.to_string(), link.to_string(), body)
        }
        Err(e) => Article::new(title.to_string(), link.to_string(), e.to_string()),
    }
}

#[catch(404)]
fn not_found(req: &Request) -> String {
    format!("Oh no! We couldn't find the requested path '{}'", req.uri())
}

lazy_static! {
    static ref PARSE_URL: String = env::var("PARSER_URL").unwrap_or("localhost".to_string());
    static ref MONGODB_URL: String = env::var("MONGODB_URL").unwrap_or("localhost".to_string());
}

#[rocket::main]
async fn main() {
    rocket::build()
        .mount("/", routes![rss_id])
        .register("/", catchers![not_found])
        .launch()
        .await;
}

#[cfg(test)]
mod tests {
    use super::*;
    use reqwest::blocking::Client as bClient;

    #[test]
    fn check_rss_link() {
        let client = bClient::new();
        let content = client
            .get("https://www.repubblica.it/rss/cronaca/rss2.0.xml")
            .send()
            .unwrap();
        let content = content.bytes().unwrap();
        let channel = Channel::read_from(&content[..]);
        println!("{:?}", channel);
        assert!(channel.is_ok());
    }
}
