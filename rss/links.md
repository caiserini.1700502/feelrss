- Corriere:
    http://xml2.corriereobjects.it/rss/cronache.xml
- Espresso:
    https://espresso.repubblica.it/rss?sezione=espresso
- La stampa:
    http://feed.lastampa.it/Homepage.rss
- Giallo Zafferano:
    https://www.giallozafferano.it/feed
- Wired:
    https://www.wired.com/feed/category/science/latest/rss
- Repubblica:
    https://www.repubblica.it/rss/cronaca/rss2.0.xml
- Ansa:
    https://www.ansa.it/sito/notizie/politica/politica_rss.xml