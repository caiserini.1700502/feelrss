#[macro_use]
extern crate rocket;
extern crate scraper;
extern crate serde_json;

mod parser;

use std::{collections::HashMap, error::Error, fs::File, io::BufReader, path::Path};

use rocket::{Request, State};
use rocket::serde::json::Json;
use rocket::serde::Deserialize;

use parser::{Parser, WebSite};

#[derive(Deserialize)]
struct Html {
    link: String,
    body: String,
}

#[catch(404)]
fn not_found(req: &Request) -> String {
    format!("Oh no! We couldn't find the requested path '{}'", req.uri())
}

#[post("/parse", data = "<html>")]
fn parse_req(parsers: &State<HashMap<String, WebSite>>,html: Json<Html>) -> String {
    let p = match parsers.keys().find(|k| html.link.contains(*k)){
        Some(p) => p.to_owned(),
        None => "wired".to_string()
    };
    parsers.get(&p).unwrap().parse(&html.body)
}

fn load_parser<P: AsRef<Path>>(path: P) -> Result<HashMap<String, Parser>, Box<dyn Error>>{
    let file = File::open(path)?;
    let reader = BufReader::new(file);

    let map = serde_json::from_reader(reader)?;

    Ok(map)
}

fn load_websites() -> HashMap<String, WebSite>{
    let map = match load_parser("data/parsers.json"){
        Ok(m) => m,
        Err(e) => {panic!("Failed to load parser\n{}",e.to_string())}
    };
    map.into_iter().map(|(key, value)| (key, value.into())).collect()
}


#[rocket::main]
async fn main() {
    let map = load_websites();

    rocket::build()
        .register("/", catchers![not_found])
        .manage(map)
        .mount("/", routes![parse_req])
        .launch()
        .await;
}







#[cfg(test)]
mod tests {
    use super::*;
    use std::fs;

    #[test]
    fn check_wired_articles() {
        let cmp = include_str!("../wired_example.html");
        //let res = res.replace(char::is_whitespace, "");
        let page = include_str!("../wired_real.html");

        //let mut page = String::new();
        let res = load_websites().get("wired").unwrap().parse(page);
        fs::write("wired_parsed.html", &res).expect("Writing page error:");
        //let page = page.replace(char::is_whitespace, "");
        assert_eq!(res.len(), cmp.len())
    }

    #[test]
    fn test_non_wired_pages() {
        let page = include_str!("../espresso-2.htm");
        let res = load_websites().get("espresso").unwrap().parse(page);
        fs::write("espresso-2_parsed.html", &res).expect("Writing page error:");
        assert!(res.bytes().len() < 1.5e+6 as usize)
    }

    #[test]
    fn test_loading_parser() {
        let map = match load_parser("./parsers.json"){
            Ok(m) => m,
            Err(e) => {panic!("Failed to load parser\n{}",e.to_string())}
        };
        let map : HashMap<String, WebSite> = map.into_iter().map(|(key, value)| (key, value.into())).collect();
        let wired : WebSite = WebSite::from_tags(&[r#"title"#], &[r#"h2[class="gallery-slide-caption__hed"],p"#/* div[class="gallery-slide-caption__dek]"# */]);
        println!("{:?}", wired);
        println!("{:?}", map.get("wired"));
        assert!(map.len() > 0);
    }
}
