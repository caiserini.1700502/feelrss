use scraper::{ElementRef, Html, Selector};
use serde::Deserialize;

#[derive(Debug)]
enum Repeat {
    Once,
    Until,
}

#[derive(Debug)]
struct Fragment {
    tags: Vec<Selector>,
    repeat: Repeat,
}

impl Fragment {
    fn new<S>(tags: &[S], repeat: Repeat) -> Self where
        S: AsRef<str>{
        Self {
            tags: tags
                .into_iter()
                .filter_map(|s| Selector::parse(s.as_ref()).ok())
                .collect(),
            repeat,
        }
    }

    fn parse(&self, html: &ElementRef) -> String {
        let (first, nested) = self.tags.split_first().unwrap();
        let mut page = html.select(first);
        let next = |p: ElementRef| -> String {
            nested
            .iter()
            .fold(p, |acc, f| acc.select(f).next().unwrap());
            p.text().collect()
        };
        match self.repeat {
            Repeat::Once => {
                let page = page.next().unwrap();
                format!("<h1>{}</h1>\n",next(page))
            }
            Repeat::Until => {
                page.into_iter().map(next).map(|txt| format!("<p>{}</p>\n", txt)).collect()
            }
        }
    }
}

#[derive(Debug)]
pub struct WebSite {
    title: Fragment,
    body: Fragment,
}

impl WebSite {

    pub fn from_tags<S: AsRef<str>>(title: &[S], body: &[S]) -> Self {
        assert!(!(title.is_empty() && body.is_empty()));
        Self {
            title: Fragment::new(title, Repeat::Once),
            body: Fragment::new(body, Repeat::Until),
        }
    }

    pub fn parse(&self, html: &str) -> String {
        let html = Html::parse_document(&html);
        let body = Selector::parse("body").unwrap();
        let head = Selector::parse("head").unwrap();
        let body = html.select(&body).next().unwrap();
        let head = html.select(&head).next().unwrap();
        format!("{}\n{}",self.title.parse(&head),self.body.parse(&body))
    }
}

impl From<Parser> for WebSite{
    fn from(p: Parser) -> Self {
        WebSite::from_tags(&p.title, &p.body)
    }
}


#[derive(Deserialize, Debug)]
pub struct Parser{
    pub title: Vec<String>,
    pub body: Vec<String>,
}