FROM rust:1.52.1-alpine3.12 as builder

RUN apk add libressl-dev pkgconf musl-dev

WORKDIR /usr/src
RUN USER=root cargo new parser
WORKDIR /usr/src/parser

COPY . .

RUN cargo build 
RUN cp target/debug/parser /usr/local/cargo/bin/parser



FROM alpine:3.12.7
RUN apk add libressl-dev

COPY --from=builder /usr/local/cargo/bin/parser /usr/local/bin/parser
COPY data/parsers.json /data/ 
COPY Rocket.toml /data/
ENV ROCKET_CONFIG=/data/Rocket.toml
CMD ["parser"]

EXPOSE 6000
